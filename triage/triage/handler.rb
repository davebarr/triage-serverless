# frozen_string_literal: true

require_relative '../processor/availability_priority'
require_relative '../processor/backstage_label'
require_relative '../processor/customer_label'
require_relative '../processor/deprecated_label'
require_relative '../processor/hackathon_label'
require_relative '../processor/doc_community_contribution'
require_relative '../processor/jihu_contribution'
require_relative '../processor/label_inference'
require_relative '../processor/merge_request_ci_title_label'
require_relative '../processor/merge_request_help'
require_relative '../processor/new_pipeline_on_approval'
require_relative '../processor/reactive_labeler'
require_relative '../processor/thank_community_contribution'
require_relative 'listener'

module Triage
  class Handler
    DEFAULT_TRIAGERS = [
      AvailabilityPriority,
      CustomerLabel,
      LabelInference,
      BackstageLabel,
      DeprecatedLabel,
      ThankCommunityContribution,
      JiHuContribution,
      DocCommunityContribution,
      MergeRequestHelp,
      ReactiveLabeler,
      NewPipelineOnApproval,
      HackathonLabel,
      MergeRequestCiTitleLabel
    ].freeze

    Result = Struct.new(:message, :error)

    def initialize(event, triagers: DEFAULT_TRIAGERS)
      @event = event
      @triagers = triagers
    end

    def process
      results = Hash.new { |h, k| h[k] = Result.new }

      listeners[event.key].each do |triager|
        results[triager.name].message = triager.triage(event)
      rescue => e
        results[triager.name].error = e
      end

      results.select { |triager, result| result.message || result.error }
    end

    private

    attr_reader :event, :triagers

    def listeners
      @listeners ||= triagers.each_with_object(Hash.new { |h, k| h[k] = [] }) do |triager, result|
        triager.listeners.each do |listener|
          result[listener.event] << triager
        end
      end
    end
  end
end
