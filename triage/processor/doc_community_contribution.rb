# frozen_string_literal: true

require_relative '../triage/triager'
require_relative '../triage/documentation_code_owner'
require_relative '../triage/unique_comment'

module Triage
  class DocCommunityContribution < Triager
    DOC_FILE_REGEX = %r{\Adocs?/}
    DOCUMENTATION_LABEL = 'documentation'
    TECHNICAL_WRITING_LABEL = 'Technical Writing'
    TECHNICAL_WRITING_TRIAGED_LABEL = 'tw::triaged'
    GL_DOCSTEAM_HANDLE = 'gl-docsteam'

    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      event.from_gitlab_org? &&
        event.wider_community_author? &&
        !event.wip? &&
        !merge_request_labelled_technical_writing? &&
        merge_request_changes_doc? &&
        unique_comment.no_previous_comment?
    end

    def process
      post_documentation_label_comment
    end

    private

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end

    def merge_request_changes_doc?
      merge_request_changes.any? do |change|
        doc_change?(change)
      end
    end

    def merge_request_labelled_technical_writing?
      event.label_names.any? do |label|
        label == TECHNICAL_WRITING_TRIAGED_LABEL ||
          label == TECHNICAL_WRITING_LABEL ||
          label.start_with?('tw::')
      end
    end

    def doc_change?(change)
      %w[old_path new_path].any? do |path|
        change[path].match(DOC_FILE_REGEX)
      end
    end

    def merge_request_changes
      Triage.api_client.merge_request_changes(project_id, merge_request_iid).changes
    end

    def project_id
      event.project_id
    end

    def merge_request_iid
      event.iid
    end

    def post_documentation_label_comment
      comment = <<~MARKDOWN.chomp
        #{message if approver_usernames.any?}
        /label ~"#{DOCUMENTATION_LABEL}" ~"#{TECHNICAL_WRITING_TRIAGED_LABEL}"
      MARKDOWN

      add_comment(comment.strip)
    end

    def message
      comment = <<~MESSAGE
        Hi #{approver_usernames.join(' ')}, please review this ~"#{DOCUMENTATION_LABEL}" Merge Request.
      MESSAGE

      if approver_usernames.include?("@#{GL_DOCSTEAM_HANDLE}")
        comment += <<~MESSAGE

          Please also consider updating the `CODEOWNERS` file in the #{event.project_web_url} project.
        MESSAGE
      end

      unique_comment.wrap(comment)
    end

    def approver_usernames
      approvers = Triage::DocumentationCodeOwner.new(project_id, merge_request_iid).approvers
      approvers.map { |username| "@#{username}"}
    end
  end
end
