# frozen_string_literal: true

require_relative '../triage/triager'

module Triage
  class CustomerLabel < Triager
    react_to 'issue.*', 'merge_request.*'

    def applicable?
      event.from_gitlab_org? &&
        has_customer_link?
    end

    def process
      add_customer_label
    end

    private

    def has_customer_link?(new_comment = event.new_comment)
      new_comment.include?('gitlab.zendesk.com') ||
        new_comment.include?('gitlab.my.salesforce.com')
    end

    def add_customer_label
      add_comment('/label ~customer')
    end
  end
end
