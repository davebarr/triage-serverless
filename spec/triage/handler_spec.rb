# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/handler'

RSpec.describe Triage::Handler do
  include_context 'with event' do
    let(:event_attrs) do
      {
        note?: false
      }
    end
  end

  let(:triager1) do
    Class.new(Triage::Triager) do
      react_to 'issue.*'

      def self.name
        'triager1'
      end

      def process
        true
      end
    end
  end
  let(:triager2) do
    Class.new(Triage::Triager) do
      react_to 'merge_request.note'

      def self.name
        'triager2'
      end

      def process
        nil
      end
    end
  end
  let(:triager3) do
    Class.new(Triage::Triager) do
      react_to '*.*'

      def self.name
        'triager3'
      end

      def process
        true
      end
    end
  end

  subject { described_class.new(event, triagers: [triager1, triager2, triager3]) }

  describe 'DEFAULT_TRIAGERS' do
    it 'includes all triager implementations' do
      expected = [
        Triage::AvailabilityPriority,
        Triage::CustomerLabel,
        Triage::LabelInference,
        Triage::BackstageLabel,
        Triage::DeprecatedLabel,
        Triage::ThankCommunityContribution,
        Triage::JiHuContribution,
        Triage::DocCommunityContribution,
        Triage::MergeRequestHelp,
        Triage::ReactiveLabeler,
        Triage::NewPipelineOnApproval,
        Triage::HackathonLabel,
        Triage::MergeRequestCiTitleLabel
      ]

      expect(described_class::DEFAULT_TRIAGERS).to eq(expected)
    end

    described_class::DEFAULT_TRIAGERS.each do |triager|
      it "reacts to something for #{triager}" do
        expect(triager.listeners).to be_any
      end
    end
  end

  describe '#process' do
    it 'executes triagers that listen to the event' do
      expect(triager1).to receive(:triage).once.and_call_original
      expect(triager2).not_to receive(:triage)
      expect(triager3).to receive(:triage).once.and_call_original

      results = subject.process

      expect(results[triager1.name].message).to eq(true)
      expect(results[triager2.name]).to be_nil
      expect(results[triager3.name].message).to eq(true)
    end

    context 'when a triager raises an error' do
      let(:error) { RuntimeError.new }

      before do
        allow(triager1).to receive(:triage).and_raise(error)
      end

      it 'captures error' do
        results = subject.process

        expect(results[triager1.name].error).to eq(error)
        expect(results[triager2.name]).to be_nil
        expect(results[triager3.name].error).to be_nil
      end

      it 'executes subsequent triager' do
        expect(triager2).not_to receive(:triage)
        expect(triager3).to receive(:triage).once

        subject.process
      end
    end
  end
end
