# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/event'
require_relative '../../triage/triage/triager'
require_relative '../../triage/triage/unique_comment'

RSpec.describe Triage::UniqueComment do
  include_context 'with event' do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'open'
      }
    end
  end

  subject { described_class.new('TestClass', event) }

  describe 'NOTES_PER_PAGE' do
    it { expect(described_class::NOTES_PER_PAGE).to eq(100) }
  end

  describe '#wrap' do
    it { expect(subject.wrap("Hello World!")).to eq("<!-- triage-serverless TestClass -->\nHello World!") }
  end

  describe '#no_previous_comment?' do
    def expect_notes_request_with(event, body:)
      response_body = body ? [{ body: body }] : []
      expect_api_request(path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes", query: { per_page: 100 }, response_body: response_body) do
        yield
      end
    end

    %w[Issue MergeRequest].each do |resource_type|
      context "with a #{resource_type} event" do
        include_context 'with event', "Triage::#{resource_type}Event" do
          let(:event_attrs) do
            {
              object_kind: resource_type.gsub(/(?<lowercase_letter>[a-z])(?<uppercase_letter>[A-Z])/, '\k<lowercase_letter>_\k<uppercase_letter>').downcase,
              issue?: resource_type == "Issue",
              merge_request?: resource_type == "MergeRequest",
              project_id: 42,
              iid: 12
            }
          end
        end

        context "when there is no previous comment" do
          it "returns true" do
            expect_notes_request_with(event, body: nil) do
              expect(subject.no_previous_comment?).to eq(true)
            end
          end
        end

        context "when there is a previous comment" do
          it "returns false" do
            expect_notes_request_with(event, body: "<!-- triage-serverless TestClass -->\nHello World!") do
              expect(subject.no_previous_comment?).to eq(false)
            end
          end
        end
      end
    end
  end
end
