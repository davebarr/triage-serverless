# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/new_pipeline_on_approval'
require_relative '../../triage/triage/event'

RSpec.describe Triage::NewPipelineOnApproval do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:approver_username) { 'approver' }
    let(:target_branch) { 'master' }
    let(:source_branch) { 'ms-viewport' }
    let(:project_id) { 123 }
    let(:merge_request_iid) { 300 }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org?: true,
        user_username: approver_username,
        iid: merge_request_iid,
        project_id: project_id,
        project_path_with_namespace: described_class::GITLAB_PROJECT_PATH,
        noteable_path: '/foo',
        payload: {
          'object_attributes' => {
            'target_branch' => target_branch,
            'source_branch' => source_branch,
          }
        }
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.approval', 'merge_request.approved']

  let(:approver_username) { 'approver' }
  let(:target_branch) { 'master' }
  let(:source_branch) { 'ms-viewport' }
  let(:project_id) { 123 }
  let(:merge_request_iid) { 300 }

  let(:event) do
    instance_double('Triage::MergeRequestEvent',
                    from_gitlab_org?: true,
                    iid: merge_request_iid,
                    project_id: project_id,
                    merge_request?: true,
                    approval_event?: true,
                    approved_event?: true,
                    gitlab_org_author?: true,
                    user_username: approver_username,
                    project_path_with_namespace: 'gitlab-org/gitlab',
                    noteable_path: '/foo',
                    payload: {
                      'object_attributes' => {
                        'target_branch' => target_branch,
                        'source_branch' => source_branch,
                      }
                    })
  end

  let(:merge_request_notes) do
    [
      { "body": "review comment 1" },
      { "body": "review comment 2" }
    ]
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/notes?per_page=100",
      response_body: merge_request_notes)
    allow(subject).to receive(:rolled_out?).and_return(true)
  end

  describe '#applicable?' do
    context 'when event is not from gitlab-org/gitlab' do
      before do
        allow(event).to receive(:project_path_with_namespace).and_return('gitlab-org/gitaly')
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not from gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request source branch is `release-tools/update-gitaly`' do
      let(:source_branch) { 'release-tools/update-gitaly' }

      include_examples 'event is not applicable'
    end

    context 'when merge request target branch is a stable branch' do
      let(:target_branch) { '14-0-stable-ee' }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    context 'when merge request author is a gitlab-org member' do
      it 'triggers a new pipeline and posts a comment to inform that a new pipeline has been triggered' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          :wave: `@#{approver_username}`, thanks for approving this merge request.

          This is the first time the merge request is approved. To ensure full test coverage, a new pipeline has been started.
          
          For more info, please refer to the following links:
          - [`rspec` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#rspec-minimal-jobs)
          - [`jest` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#jest-minimal-jobs)
          - [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).
        MARKDOWN

        expect_api_requests do |requests|
          requests << stub_api_request(verb: :post, path: "#{event.noteable_path}/pipelines")
          requests << stub_comment_request(event: event, body: body)

          subject.process
        end
      end
    end

    context 'when merge request author is not a gitlab-org member' do
      before do
        allow(event).to receive(:gitlab_org_author?).and_return(false)
      end

      it 'posts a discussion to nudge the approver to start a new pipeline' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          :wave: `@#{approver_username}`, thanks for approving this merge request.

          This is the first time the merge request is approved. To ensure full test coverage, please start a new pipeline before merging.
          
          For more info, please refer to the following links:
          - [`rspec` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#rspec-minimal-jobs)
          - [`jest` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#jest-minimal-jobs)
          - [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).
        MARKDOWN

        expect_discussion_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
