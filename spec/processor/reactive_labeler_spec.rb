# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/reactive_labeler'

RSpec.describe Triage::ReactiveLabeler do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        object_kind: 'note',
        noteable_type: 'merge_request',
        from_gitlab_org?: true,
        wider_community_author?: true,
        note?: true,
        new_entity?: false,
        noteable_author_id: 1,
        noteable_path: '/foo',
        new_comment: %(#{described_class::GITLAB_BOT} label #{labels})
      }
    end
    let(:labels) { '~"group::import"' }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.note', 'merge_request.note']

  describe 'GITLAB_BOT' do
    it { expect(described_class::GITLAB_BOT).to eq('@gitlab-bot') }
  end

  describe 'ACTION_PREFIX_REGEXP' do
    it { expect(described_class::ACTION_PREFIX_REGEXP).to eq(/^#{Regexp.escape(described_class::GITLAB_BOT)}[[:space:]]+label[[:space:]]+/) }
  end

  describe 'ACTION_REGEXP' do
    it { expect(described_class::ACTION_REGEXP).to eq(/#{described_class::ACTION_PREFIX_REGEXP}.+/) }
  end

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not mentioning the bot' do
      before do
        allow(event).to receive(:new_comment).and_return('@foo label ~bar')
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not using the "label" action' do
      before do
        allow(event).to receive(:new_comment).and_return("#{described_class::GITLAB_BOT} unlabel ~bar")
      end

      include_examples 'event is not applicable'
    end

    context 'when event is for a non-allowed label' do
      context 'when label is ~"pick into x"' do
        let(:labels) { '~"pick into x"' }

        include_examples 'event is not applicable'
      end

      context 'when label is ~"group::import::nested"' do
        let(:labels) { '~"group::import::nested"' }

        include_examples 'event is not applicable'
      end
    end

    context 'when event is for a new merge request mentioning the bot' do
      before do
        allow(event).to receive(:note?).and_return(false)
        allow(event).to receive(:new_entity?).and_return(true)
        allow(event).to receive(:merge_request?).and_return(true)
      end

      context 'when label is ~"group::import"' do
        it_behaves_like 'event is applicable'
      end
    end

    context 'when event is for a new note mentioning the bot' do
      context 'when event author is not the noteable author' do
        before do
          allow(event).to receive(:noteable_author_id).and_return(2)
        end

        include_examples 'event is not applicable'
      end

      context 'when label is ~"group::import"' do
        it_behaves_like 'event is applicable'
      end
    end
  end

  describe '#process' do
    shared_examples 'message posting' do |expected_labels|
      it 'posts /label command' do
        body = <<~MARKDOWN.chomp
          /label #{expected_labels || labels}
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when event is for a new issue mentioning the bot' do
      before do
        allow(event).to receive(:note?).and_return(false)
        allow(event).to receive(:new_entity?).and_return(true)
        allow(event).to receive(:issue?).and_return(true)
      end

      context 'when label is ~"group::import"' do
        it_behaves_like 'message posting'
      end

      context 'when command is not at the beginning of the description' do
        before do
          allow(event).to receive(:new_comment).and_return(%(Hello\n#{described_class::GITLAB_BOT} label #{labels}\nWorld!))
        end

        it_behaves_like 'message posting'

        context 'with multiple labels on the same line with extra spaces' do
          let(:labels) { '  ~"group::import"   ~"group::source code"   ' }

          it_behaves_like 'message posting', '~"group::import" ~"group::source code"'
        end

        context 'with multiple commands on separate lines' do
          before do
            allow(event).to receive(:new_comment).and_return(%(Hello\n#{described_class::GITLAB_BOT} label #{labels}\nWorld!\n#{described_class::GITLAB_BOT} label ~group::foo))
          end

          it_behaves_like 'message posting', '~"group::import" ~"group::foo"'
        end
      end
    end

    it_behaves_like 'rate limited'
  end
end
