# frozen_string_literal: true

require 'json'
require 'webmock/rspec'
require 'timecop'
require 'rspec-parameterized'

require_relative '../triage/triage'

Dir[File.expand_path("support/**/*.rb", __dir__)].sort.each { |f| require f }

RSpec.configure do |config|
  config.filter_run focus: true
  config.run_all_when_everything_filtered = true

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  module Helper
    def stub_env(mapping)
      allow(ENV).to receive(:[]).and_call_original

      mapping.each do |key, value|
        allow(ENV).to receive(:[]).with(key).and_return(value.to_s)
      end
    end

    def expect_api_requests
      requests = []

      yield requests

      expect(requests).to all(have_been_requested)

      WebMock.reset_executed_requests!
    end

    def expect_api_request(times: 1, **arguments)
      request = stub_api_request(**arguments)

      yield

      expect(request).to have_been_requested.times(times)
      WebMock.reset_executed_requests!
    end

    def expect_comment_request(times: 1, **arguments)
      request = stub_comment_request(**arguments)

      yield

      expect(request).to have_been_requested.times(times)
      WebMock.reset_executed_requests!
    end

    def expect_discussion_request(times: 1, **arguments)
      request = stub_discussion_request(**arguments)

      yield

      expect(request).to have_been_requested.times(times)
      WebMock.reset_executed_requests!
    end

    def expect_no_request(**arguments)
      request = stub_api_request(**arguments)

      yield

      expect(request).not_to have_been_requested
      WebMock.reset_executed_requests!
    end

    def stub_api_request(verb: :get, path: nil, query: {}, request_body: {}, response_body: {})
      request_params = { headers: { 'Private-Token': api_token } }
      request_params[:query] = query if query&.any?
      request_params[:body] = request_body if request_body&.any?

      stub_request(verb, "#{Triage::PRODUCTION_API_ENDPOINT}#{path}")
        .with(request_params)
        .to_return(body: response_body.to_json)
    end

    def stub_comment_request(event:, body:)
      stub_api_request(verb: :post, path: "#{event.noteable_path}/notes", request_body: { 'body' => body })
    end

    def stub_discussion_request(event:, body:)
      stub_api_request(verb: :post, path: "#{event.noteable_path}/discussions", request_body: { 'body' => body })
    end

    def api_token
      'api'
    end

    def webhook_token
      'webhook'
    end
  end

  config.include(Helper)

  config.before do
    stub_env(
      'GITLAB_API_TOKEN' => api_token,
      'GITLAB_WEBHOOK_TOKEN' => webhook_token
    )
  end

  config.before(:each, :clean_cache) do
    Triage.cache.reset
  end
end
