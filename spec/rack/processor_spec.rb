# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/rack/processor'

describe Triage::Rack::Processor do
  include_context 'with event'

  let(:app)      { described_class.new }
  let(:env)      { { event: event } }
  let(:handler)  { double('Triage::Handler') }
  let(:response) { app.call(env) }
  let(:status)   { response[0] }
  let(:body)     { response[2][0] }

  before do
    allow(Triage::Handler).to receive(:new).with(event).and_return(handler)
    allow(app).to receive(:puts)
    allow(handler).to receive(:process)
  end

  context 'when Triage::ClientError is thrown' do
    it 'returns a 400 error' do
      expect(handler).to receive(:process).and_raise(Triage::ClientError)

      expect(status).to eq(400)
      expect(body).to eq(JSON.dump(status: :error, error: "Triage::ClientError", message: "Triage::ClientError"))
    end
  end

  context 'when an error is thrown' do
    let(:error) { StandardError.new('runtime error') }

    before do
      allow(handler).to receive(:process).and_raise(error)
    end

    it 'returns a 500 error and tags Sentry error with event type' do
      expect(Raven).to receive(:capture_exception).with(error)
      expect(Raven).to receive(:tags_context).with(object_kind: 'RSpec::Mocks::InstanceVerifyingDouble')
      expect(Raven).to receive(:extra_context).with(triager: nil, payload: event.payload)

      expect(status).to eq(500)
      expect(body).to eq(JSON.dump(status: :error, error: "StandardError", message: "runtime error"))
    end
  end

  context 'when no error is thrown' do
    let(:message) { 'foo' }
    let(:result) do
      {
        'triager' => double('Result', message: message, error: nil)
      }
    end

    before do
      allow(handler).to receive(:process).and_return(result)
    end

    it 'returns a 200 response with the messages from the processors' do
      expect(status).to eq(200)
      expect(body).to eq(JSON.dump(status: :ok, messages: { 'triager' => message }))
    end
  end

  context 'when triagers encountered errors' do
    let(:error1) { StandardError.new('runtime error 1') }
    let(:error2) { StandardError.new('runtime error 2') }
    let(:result) do
      {
        'triager1' => double('Result', message: 'first success', error: error1),
        'triager2' => double('Result', message: nil, error: error2)
      }
    end

    before do
      allow(handler).to receive(:process).and_return(result)
    end

    it 'returns a 200 response and tags Sentry error with event type' do
      expect(Raven).to receive(:tags_context).with(object_kind: 'RSpec::Mocks::InstanceVerifyingDouble').ordered
      expect(Raven).to receive(:extra_context).with(triager: 'triager1', payload: event.payload).ordered
      expect(Raven).to receive(:capture_exception).with(error1).ordered

      expect(Raven).to receive(:tags_context).with(object_kind: 'RSpec::Mocks::InstanceVerifyingDouble').ordered
      expect(Raven).to receive(:extra_context).with(triager: 'triager2', payload: event.payload).ordered
      expect(Raven).to receive(:capture_exception).with(error2).ordered

      expect(status).to eq(200)
      expect(body).to eq(JSON.dump(status: :ok, messages: { 'triager1' => 'first success' }))
    end
  end
end
