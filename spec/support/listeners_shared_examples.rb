RSpec.shared_examples 'registers listeners' do |listeners|
  let(:triager) { described_class }

  describe '.listeners' do
    it 'processes different conditions' do
      expect(triager.listeners.map(&:event)).to match_array(listeners)
    end
  end
end
