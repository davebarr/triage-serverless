RSpec.shared_context 'with event' do |event_class = 'Triage::Event'|
  let(:event_attrs) { {} }
  let(:event) do
    instance_double(event_class, {
      object_kind: 'issue',
      action: 'open',
      user: { 'id' => 1, 'username' => 'root' },
      author_id: 42,
      key: 'issue.open',
      payload: {}
    }.merge(event_attrs))
  end
end
