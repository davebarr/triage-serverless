stages:
  - test
  - build
  - secrets
  - deploy
  - health-check

workflow:
  rules:
    - if: '$FORCE_GITLAB_CI'
    - if: '$CI_MERGE_REQUEST_IID'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

default:
  image: ruby:2.7

variables:
  SERVICE_TYPE: web
  IMAGE_PATH: "${CI_REGISTRY}/${CI_PROJECT_PATH}:${CI_COMMIT_REF_SLUG}"
  DOCKER_VERSION: "20.10.1"

.rules-deploy:
  rules:
    - if: '$SINGLE_JOB'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - when: never # Remove this in a merge request if deploy is needed there
    - when: manual

.rules-general:
  rules:
    - if: '$SINGLE_JOB'
      when: never
    - when: on_success

.use-docker-in-docker:
  image: docker:${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  tags:
    # See https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7019 for tag descriptions
    - gitlab-org-docker

unit:tests:
  stage: test
  extends: .rules-general
  script:
    - bundle install
    - bundle exec rspec --force-color

build:triage-web:
  stage: build
  extends: .rules-general
  image:
    name: gcr.io/kaniko-project/executor:debug-v1.3.0
    entrypoint: [""]
  before_script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
  script:
    - /kaniko/executor --context=${CI_PROJECT_DIR} --dockerfile=${CI_PROJECT_DIR}/Dockerfile.rack --destination=${IMAGE_PATH} --cache=true

deploy:secrets:
  stage: secrets
  environment: production
  extends: .rules-deploy
  needs: []
  image: registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/2.16.9-kube-1.13.12
  script:
    - kubectl create secret generic gitlab-webhook-token
        --from-literal GITLAB_WEBHOOK_TOKEN="$GITLAB_WEBHOOK_TOKEN"
        --namespace "$KUBE_NAMESPACE"
        --dry-run -o yaml | kubectl apply -f -
    - kubectl create secret generic gitlab-api-token
        --from-literal GITLAB_API_TOKEN="$GITLAB_API_TOKEN"
        --namespace "$KUBE_NAMESPACE"
        --dry-run -o yaml | kubectl apply -f -
    - kubectl create secret generic sentry-dsn
        --from-literal SENTRY_DSN="$SENTRY_DSN_PRODUCTION"
        --namespace "$KUBE_NAMESPACE"
        --dry-run -o yaml | kubectl apply -f -
    - kubectl create secret generic slack-webhook-url
        --from-literal SLACK_WEBHOOK_URL="$SLACK_WEBHOOK_URL"
        --namespace "$KUBE_NAMESPACE"
        --dry-run -o yaml | kubectl apply -f -
    - kubectl create secret generic user-account-csv-url
        --from-literal USER_ACCOUNT_CSV_URL="$USER_ACCOUNT_CSV_URL"
        --namespace "$KUBE_NAMESPACE"
        --dry-run -o yaml | kubectl apply -f -
    - kubectl create secret generic alt-project-id-for-ci-title-label
        --from-literal ALT_PROJECT_ID_FOR_CI_TITLE_LABEL="$ALT_PROJECT_ID_FOR_CI_TITLE_LABEL"
        --namespace "$KUBE_NAMESPACE"
        --dry-run -o yaml | kubectl apply -f -

deploy:triage-web:
  stage: deploy
  environment: production
  extends: .rules-deploy
  needs: ["build:triage-web"]
  image: registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/2.16.9-kube-1.13.12
  script:
    - echo ${KUBE_NAMESPACE}
    - kubectl apply -f config/triage-web.yaml -n ${KUBE_NAMESPACE}
    - kubectl set image deployment/triage-web triage-web=${IMAGE_PATH} -n ${KUBE_NAMESPACE}
    # - kubectl rollout restart deployment/triage-web -n ${KUBE_NAMESPACE} # Only in kubectl 1.5+
    - kubectl scale deployment triage-web --replicas=0 -n ${KUBE_NAMESPACE}
    - kubectl scale deployment triage-web --replicas=1 -n ${KUBE_NAMESPACE}
    - kubectl get service -n ${KUBE_NAMESPACE}
    - kubectl get pods -n ${KUBE_NAMESPACE}

.health-check-report: &health-check-report
  - |
    if [[ -f health-check-failed ]]; then
      echo
      bin/slack ${NOTIFY_CHANNEL} ":siren-siren: https://triage-serverless.gitlab.com is down :siren-siren: ${CI_JOB_URL}" boom
      exit 1
    fi

health-check:container:
  stage: health-check
  extends: .use-docker-in-docker
  variables:
    CONTAINER_NAME: triage-reactive
  before_script:
    - export SERVICE_URL=http://${CONTAINER_NAME}:8080
    - docker network create test
    - docker run -d -p 8080:8080 --name ${CONTAINER_NAME} --net test --hostname ${CONTAINER_NAME} -e GITLAB_WEBHOOK_TOKEN=gitlab_webhook_token -e GITLAB_API_TOKEN=gitlab_api_token -e SLACK_WEBHOOK_URL='' -e DRY_RUN=1 ${IMAGE_PATH}
    - docker ps
    - docker logs -f ${CONTAINER_NAME} &> container.log &
  script:
    - docker run --rm --net test -v "$PWD:/work" curlimages/curl /work/bin/health-check $SERVICE_URL /work gitlab_webhook_token
    - docker stop ${CONTAINER_NAME}
    - docker network rm test
    - test ! -f health-check-failed
  artifacts:
    paths:
      - container.log
    expire_in: 31d
    when: always

health-check:service:
  stage: health-check
  image: curlimages/curl
  rules:
    # Schedule for it:
    # https://gitlab.com/gitlab-org/quality/triage-serverless/-/pipeline_schedules/84419/edit
    - if: '$HEALTH_CHECK'
  variables:
    SERVICE_URL: https://triage-serverless.gitlab.com
    NOTIFY_CHANNEL: g_qe_engineering_productivity
  script:
    - bin/health-check $SERVICE_URL
    - *health-check-report
